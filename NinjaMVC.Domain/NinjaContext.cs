﻿using System.Data.Entity;

namespace NinjaMVC.Domain
{
    public class NinjaContext : DbContext
    {
        public NinjaContext() : base("localhost") { } 

        public DbSet<Ninja> Ninjas { get; set; }
    }
}