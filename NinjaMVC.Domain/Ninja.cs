﻿using System.ComponentModel.DataAnnotations;

namespace NinjaMVC.Domain
{
    public class Ninja
    {
        [Key]
        [Display(Name = "ID")]
        public int Identifier { get; set; }
        
        [Display(Name="Name")]
        public string Name { get; set; }

        [Display(Name="Postal Code")]
        public string PostalCode { get; set; }

        [Display(Name="House Number")]
        public int HouseNumber { get; set; }
    }
}
