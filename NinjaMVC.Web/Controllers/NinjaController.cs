﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NinjaMVC.Domain;

namespace NinjaMVC.Web.Controllers
{
    public class NinjaController : Controller
    {
        // GET: Ninja
        public ActionResult Index()
        {
            List<Ninja> ninjas;
            using (var ctx = new NinjaContext())
            {
                ninjas = ctx.Ninjas.ToList();
            }
            return View(ninjas);
        }

        //GET: Ninja by ID
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Ninja ninja;
            using (var ctx = new NinjaContext())
            {
                ninja = ctx.Ninjas.Find(id);
            }

            return ninja == null ? HttpNotFound() : (ActionResult)View(ninja);
        }

        //GET Create Ninja form
        public ActionResult Create() => View();

        //POST Created Ninja
        [HttpPost]
        public ActionResult Create(Ninja newNinja)
        {
            using (var ctx = new NinjaContext())
            {
                ctx.Ninjas.Add(newNinja);
                ctx.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}